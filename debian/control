Source: libmojolicious-plugin-oauth2-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Frédéric Bonnard <frediz@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libcrypt-openssl-bignum-perl <!nocheck>,
                     libcrypt-openssl-rsa-perl <!nocheck>,
                     libio-socket-ssl-perl <!nocheck>,
                     libmojo-jwt-perl (>= 0.09) <!nocheck>,
                     libmojolicious-perl (>= 8.25) <!nocheck>,
                     perl
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libmojolicious-plugin-oauth2-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libmojolicious-plugin-oauth2-perl.git
Homepage: https://metacpan.org/release/Mojolicious-Plugin-OAuth2
Rules-Requires-Root: no

Package: libmojolicious-plugin-oauth2-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libio-socket-ssl-perl,
         libmojolicious-perl (>= 8.25)
Recommends: libcrypt-openssl-bignum-perl,
            libcrypt-openssl-rsa-perl,
            libmojo-jwt-perl (>= 0.09)
Description: Auth against OAuth2 APIs
 Mojolicious::Plugin::OAuth2 allows you to easily authenticate against a
 OAuth2 provider. It includes configurations for a few popular providers,
 but you can add your own easily as well.
 .
 Note that OAuth2 requires https, so you need to have the optional
 Mojolicious dependency required to support it.
